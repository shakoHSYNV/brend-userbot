BREND="\n✨𝘽𝙧𝙚𝙣𝙙 𝑼𝒔𝒆𝒓𝒃𝒐𝒕 𝑰𝒏𝒔𝒕𝒂𝒍𝒍𝒆𝒓✨"
BREND+="\n "
BREND+="\n📱Brend Userbot  Avtomatik Qurulum📱"
BREND+="\n⚡ Bizi Seçdiyiniz üçün təşəkkür edirik ⚡"
BREND+="\n "
BREND+="\n📢 Əsas Kanal: @BrendUserbot"
BREND+="\n⚙️ Kömək Qrupu: @BrendSUP"
BREND+="\n🧩 Plugin Kanalı: @BrendPlugin"
BREND+="\n "
BOSLUQ="\n "
echo -e $BREND
echo -e $BOSLUQ
echo "🤖 Lazımlı Termux tənzimləmələrini edirəm"
echo -e $BOSLUQ
pkg update -y && pkg upgrade
clear
echo -e $BREND
echo -e $BOSLUQ
echo "💠 Cihazınıza Python Qurulur"
echo -e $BOSLUQ
pkg install python3
pip3 install --upgrade pip
cle
echo -e $BREND
echo -e $BOSLUQ
echo "⚙️ Github Tənzimlənmələri aparılır"
echo -e $BOSLUQ
pkg install git -y
rm -rf brend
clear
echo -e $BREND
echo -e $BOSLUQ
echo "⚡ Brend Userbot Yüklənir"
echo -e $BOSLUQ
git clone https://github.com/brendsupport/brend
clear
echo -e $BREND
echo -e $BOSLUQ
echo "⛓️ Lazımlı kiçik fayllar yüklənir"
echo -e $BOSLUQ
cd brend
pip3 install -U -r requirements.txt
python3 -m brend_installer
